CC = gcc
CFLAGS = -I -Wall -Werror -Wshadow -pedantic -std=c11 -O0
DEPS = helpers.c

bubble_sort: bubble_sort.c $(DEPS)
	$(CC) $(CFLAGS) bubble_sort.c $(DEPS) -o bubble_sort

selection_sort: selection_sort.c $(DEPS)
	$(CC) $(CFLAGS) selection_sort.c $(DEPS) -o selection_sort
